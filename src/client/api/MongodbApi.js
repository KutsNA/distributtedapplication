import axios from "axios";
import { mongoDBApiUrl } from "./config";

export default class MongoDBApi {

    static async addNewPost(post) {
        const {id, postTypeId, body, ownerUserId, title, tags} = post;
        const options = {
            method: "PUT",
            header: { "content-type": "application/x-www-form-urlencoded" },
            data: `Id=${id}&PostTypeId=${postTypeId}&Body=${body}&OwnerUserId=${ownerUserId}&Title=${title}&Tags=${tags}`,
            url: `${mongoDBApiUrl}/addpost`,
        };
        await axios(options);
    };

    static async editPost(post) {
        const {Id, PostTypeId, Body, OwnerUserId, Title, Tags} = post;
        const options = {
            method: "PUT",
            header: { "content-type": "application/x-www-form-urlencoded" },
            data: `Id=${Id}&PostTypeId=${PostTypeId}&Body=${Body}&OwnerUserId=${OwnerUserId}&Title=${Title}&Tags=${Tags}`,
            url: `${mongoDBApiUrl}/editpost`,
        };
        await axios(options);
    };

    static async deletePost(id) {
        return await axios.delete(`${mongoDBApiUrl}/delete/${id}`);
    };

    static async findPostById(id) {
        return await axios
            .get(`${mongoDBApiUrl}/postid/${id}`)
            .then(response => response.data);
    };
};
//data: `id=${id}&postTypeId=${postTypeId}&body=${body}&ownerUserId=${ownerUserId}&title=${title}&tags=${tags}`,