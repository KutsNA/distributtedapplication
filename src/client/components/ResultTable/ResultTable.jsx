import React from "react";
import PropTypes from 'prop-types';
import {Table} from "semantic-ui-react";
import ResultTableRow from './ResultTableRow';

export default function ResultTable(props) {
    const {tableName, post} = props;
    return (
        <Table celled>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>{tableName}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                <Table.Row>
                    <Table.Cell>
                        <ResultTableRow data={post}/>
                    </Table.Cell>
                </Table.Row>
            </Table.Body>
        </Table>
);
}

ResultTable.PropTypes = {
    tableName: PropTypes.string,
    post: PropTypes.shape()
};

ResultTable.defaltProps = {
    tableName: 'Result Table',
    post: null
};
