import React from "react";
import PropTypes from "prop-types";
import {Grid, Form, Input, Button, Label, List, Icon, Table} from "semantic-ui-react";

export default function ResultTableRow(props) {
    let {data} = props;
    if (data)
        data = data[0];
    const renderBody = (
        data &&
        <List>
            {
                Object.keys(data).map(key => {
                    return (
                        <List.Item key={key}>
                            {key}: {data[key]}
                        </List.Item>
                    );
                })
            }
        </List>
    );

    return renderBody ? renderBody : null;
}

ResultTableRow.PropTypes = {
    data: PropTypes.shape()
};

ResultTableRow.defaltProps = {
    data: null
};

/*

 */