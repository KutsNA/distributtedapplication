import React from "react";
import { Input, Button, Label, Icon } from "semantic-ui-react";
import PropTypes from 'prop-types';

export default function SearchFormTemplate (props) {

    const {field, handleChange, createRequest} = props;

    return (
        <Input
            label={
                <Label>
                    <Icon name="stack overflow"/>
                </Label>
            }
            name={field.name}
            value={field.value}
            onChange={handleChange}
            placeholder={field.name}
            action={
                <Button
                    color="teal"
                    icon="search"
                    content="Find posts"
                    onClick={() => createRequest()}
                />
            }/>
    );
};

SearchFormTemplate.PropTypes = {
    field: PropTypes.shape({
        name: PropTypes.string.isRequire,
        value: PropTypes.string.isRequire
    }),
    handleChange: PropTypes.func.isRequire,
    createRequest: PropTypes.func.isRequire
};