import React, {Component} from "react";
import {Grid, Form, Input, Button, Label, List, Icon, Table} from "semantic-ui-react";
import SearchFormTemplate from '../components/SearchFormTemplate';
import ResultTable from '../components/ResultTable/ResultTable';
import MongodbApi from '../api/MongodbApi';
import PostFormTemplate from './PostFormTemplate';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postId: {
                name: 'postId',
                value: ''
            },
            postBody: '',
            postFoundById: undefined,
            postsFoundByBody: undefined
        };
    }

    handlePostIdInput = (event, {name, value}) => {
        //const isInteger = /^[0-9]+$/;
        const {postId} = this.state;
        postId.value = value;
        this.setState(postId);
        //console.log(this.state);
    };

    requestPostById = async () => { // mongoDB search
        this.clearPostIdTable();
        const {postId} = this.state;
        const posts = await MongodbApi.findPostById(postId.value);
        console.log(posts);
        posts ? this.setState({postFoundById: posts}) : this.setState({postFoundById: "Nothing was found"});
    };

    deletePost = async () => {
        const {postId} = this.state;
        const response = await MongodbApi.deletePost(postId.value);
        this.clearPostIdTable();
    };

    editPost = async (post) => {
        console.log('start edditing');
        console.log(post);
        await MongodbApi.editPost(post);
    };


    clearPostIdTable = () => {
        this.setState({postFoundById: undefined});
    };

    createPost = async (post) => { // add new post to MongoDB
        console.log(post);
        await MongodbApi.addNewPost(post);
        console.log('added new post: ' + post);
    };

    render() {
        const {postId, postFoundById} = this.state;
        //console.log(this.state);
        return (
            <div>
                <Grid columns={2} divided>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <SearchFormTemplate field={postId}
                                                handleChange={this.handlePostIdInput}
                                                createRequest={this.requestPostById}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <ResultTable tableName='Post from MongoDB' post={postFoundById}/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <PostFormTemplate postAction={this.createPost} label='create post' />
                            <PostFormTemplate postAction={this.editPost} label='edit post' post={postFoundById}/>
                            <Button icon='edit' onClick={this.deletePost} label='delete post'/>
                        </Grid.Column>
                    </Grid.Row>

                </Grid>
            </div>
        );
    }
}

// 30 <!--<PostFormTemplate/>-->
export default MainPage;