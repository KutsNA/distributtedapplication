import React, {Component} from 'react';
import {Button, Form, Popup} from 'semantic-ui-react';
import PropTypes from 'prop-types';

class PostFormTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: {
                id: '',
                postTypeId: '',
                creationDate: new Date().toLocaleDateString(),
                score: 0,
                viewCount: 0,
                body: '',
                ownerUserId: '',
                title: '',
                tags: '',
            },
            isOpen: false
        };
    };

    componentWillReceiveProps(nextProps, nextContext) {
        const {post} = nextProps;
        if(post){
            this.setState({post: post[0]});
        }
    };

    handleOpen = () => {
        this.setState({isOpen: true});
    };

    handleClose = () => {
        this.setState({isOpen: false});
    };

    handleChange = (event) => {
        const {name, value} = event.target;
        const {post} = this.state;
        post[name] = value;
        this.setState(post);
    };

    handleSubmit = async () => {
        const {post} = this.state;
        const {postAction} = this.props;
        try {
            await postAction(post);
            this.handleClose();
            //this.dropToDefault();  TODO make drop to default method?
        } catch (error) {
            console.log(error);
        }
    };

    render() {
        const {isOpen, post} = this.state;
        const {label} = this.props;
        return (
            <Popup
                trigger={<Button icon='user circle' label={label} onClick={this.handleClose}/>}
                content={
                    post ?
                        <div>
                            <Form onSubmit={this.handleSubmit}>
                                {
                                    Object.keys(post).map(key => {
                                        return <Form.Input placeholder={key} name={key} value={post[key]}
                                                           onChange={this.handleChange}/>
                                    })
                                }
                                <Form.Button content={label}/>
                            </Form>
                        </div> :
                        null
                }
                open={isOpen}
                onOpen={this.handleOpen}
                onClose={this.handleClose}
                on='click'
            />
        );
    }
}

PostFormTemplate.propTypes = {
    postAction: PropTypes.func.isRequired,
    label: PropTypes.string,
};
PostFormTemplate.defaultProps = {
    label: 'Post form button',
};
export default PostFormTemplate;