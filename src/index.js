import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from './client/containers/MainPage';

ReactDOM.render(<MainPage />, document.getElementById('root'));