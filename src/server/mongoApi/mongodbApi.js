const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

const url = "mongodb://localhost:27017";
const dbName = "MongoDB";
const client = new MongoClient(url);
const collectionName = "documents";

module.exports = {

    getPostById: function(postId) {
        console.log(postId);
        if (postId) {
            return new Promise((resolve, reject) => {
                client.connect(async (err, db) => {
                    assert.equal(err, null);
                    console.log("Connected successfully to server");
                    db = client.db(dbName);
                    let post = await findPost(db, postId);
                    resolve(post);
                }, () => {
                    client.close();
                });
            });
        }
    },

    addNewPost: async function(post) {
        client.connect(function(err) {
            const db = client.db(dbName);
            insertObject(db, 'posts', post);
        });
    },

    deletePots: async function(id) {
        client.connect(async function(err) {
            const db = client.db(dbName);
            await db.collection('posts').deleteOne({'Id' : id});
        });
    },

    editPots: async function(post) {
        const id = post.Id;
        client.connect(async function(err) {
            const db = client.db(dbName);
            const res =  await db
                .collection('posts')
                .updateOne(
                    {'Id' : id},
                    {$set: post},
                    false,
                    );
        });
    }
};

async function findPost(db, postId) {
    const collection = db.collection('posts');
    const response = await collection.find({ Id: postId }).toArray();
    return response;
};

async function insertObject(db, collectionName, objectToInsert) {
    await db.collection(collectionName).insertOne(objectToInsert);
}

//export default MongodbApi;
//module.exports = MongodbApi;