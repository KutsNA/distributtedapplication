const express = require('express');
const router = express.Router();
const MongodbApi = require('../mongoApi/mongodbApi');

router.put('/', async function(req, res) {
    const post = req.body;
    if (post)
        await MongodbApi.addNewPost(post);
    //res.send({"data": "You re trying to get a post by id!"});
});

module.exports = router;