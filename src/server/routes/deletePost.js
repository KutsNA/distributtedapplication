const express = require('express');
const router = express.Router();
const {deletePots} = require('../mongoApi/mongodbApi');

/* eslint-enable no-lone-blocks */
router.delete('/:id', async function(req, res) {
    const {id} = req.params;
    console.log(id);
    if(id)
        res.send(await deletePots(req.params.id));
});

module.exports = router;