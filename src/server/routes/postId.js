const express = require('express');
const router = express.Router();
const {getPostById} = require('../mongoApi/mongodbApi');

/* eslint-enable no-lone-blocks */
router.get('/:id', async function(req, res) {
    try {
        const post = await getPostById(req.params.id);
        res.send(post);
    } catch (e) {
        console.log(e);
        res.send({"data": "You re trying to get a post by id!"});
    }

});

module.exports = router;