const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");

const url = "mongodb://localhost:27017";
const dbName = "MongoDB";
const client = new MongoClient(url);
const collectionName = "posts";

module.exports = {

    getPostById: function(postId) {
        console.log(postId);
        if (postId) {
            return new Promise((resolve, reject) => {
                client.connect(async (err, db) => {
                    assert.equal(err, null);
                    console.log("Connected successfully to server");
                    db = client.db(dbName);

                    let post = await findPost(db, postId);
                    resolve(post);
                }, () => {
                    client.close();
                });
            });
        }
    },

    addNewPost: async function(post) {
        client.connect(function(err) {
            const db = client.db(dbName);
            insertObject(db, collectionName, post);
        });
    }
};

async function findPost(db, postId) {
    const collection = db.collection("posts");
    const response = await collection.find({ Id: postId }).toArray();
    return response;
};

async function insertObject(db, collectionName, objectToInsert) {
    await db.collection(collectionName).insertOne(objectToInsert);
}

//export default MongodbApi;
//module.exports = MongodbApi;

client.connect(async function(err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    const db = client.db(dbName);

    //const response = await findAllPosts(db, 'documents');
    const post = {
        id: 777,
        postTypeId: 1,
        body: 'here is some body',
        ownerUserId: '',
        title: 'mongo',
        tags: 'mongo'
    };
    await insertObject(db, 'documents', post);
    //console.log(response);
});


async function findAllPosts(db, collectionName) {
    return await db.collection(collectionName).find().toArray(function(err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs);
    });
    ;
};